package ru.t1.skasabov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.dto.TaskDto;

public interface TaskDtoRepository extends JpaRepository<TaskDto, String> {

    @Transactional
    void deleteByProjectId(@NotNull String projectId);

}
