package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

import java.util.Collection;

@Controller
public final class TaskController {

    @Autowired
    private TaskDtoRepository taskRepository;

    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        taskRepository.save(new TaskDto("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") @NotNull final String id) {
        taskRepository.deleteById(id);
        return "redirect:/tasks";
    }

    @NotNull
    @PostMapping("/task/edit/{task.id}")
    public String edit(@ModelAttribute("task") @NotNull final TaskDto task) {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") @NotNull final String id) {
        @Nullable final TaskDto task = taskRepository.findById(id).orElse(null);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", getStatuses());
        modelAndView.addObject("projects", getProjects());
        return modelAndView;
    }

    @NotNull
    private Status[] getStatuses() {
        return Status.values();
    }

    @NotNull
    private Collection<ProjectDto> getProjects() {
        return projectRepository.findAll();
    }

}
