package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.TaskRestEndpoint;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public final class TaskEndpointImpl implements TaskRestEndpoint {

    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public TaskDto save(@NotNull @RequestBody final TaskDto task) {
        return taskRepository.save(task);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public TaskDto findById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskRepository.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final TaskDto task) {
        taskRepository.delete(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<TaskDto> tasks) {
        taskRepository.deleteAll(tasks);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        taskRepository.deleteAll();
    }

}
