package ru.t1.skasabov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.TaskDto;

import java.util.List;

public interface TaskRestEndpoint {

    @NotNull
    List<TaskDto> findAll();

    @NotNull
    TaskDto save(TaskDto task);

    @Nullable
    TaskDto findById(@NotNull String id);

    boolean existsById(@NotNull String id);

    long count();

    void deleteById(@NotNull String id);

    void delete(@NotNull TaskDto task);

    void clear(@NotNull List<TaskDto> tasks);

    void clear();

}
