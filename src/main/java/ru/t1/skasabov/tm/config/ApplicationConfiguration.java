package ru.t1.skasabov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.skasabov.tm")
public final class ApplicationConfiguration {

}
